/******************************************************************************
 * AUTHOR: Maria Vanessa Vengco <vengcom@oregonstate.edu>
 * DATE: November 23, 2015
 * FILENAME: StringList.cpp
 * DESCRIPTION: Project 9.a - The implementation file for the StringList Class.
 * It defines a simple linked list class, using an appropriate definition of
 * ListNode, to create and manipulate a list of strings.
 ******************************************************************************/

#include "StringList.hpp"
#include <string>
#include <vector>
#include <iostream>


/******************************************************************************
 *                             DEFAULT CONSTRUCTOR
 * DESCRIPTION: Initializes a new empty StringList object 
 ******************************************************************************/
StringList::StringList() : head(NULL) {}

/******************************************************************************
 *                              COPY CONSTRUCTOR
 * DESCRIPTION: Creates a new, completely separate duplicate of a StringList
 * object, a deep copy.
 ******************************************************************************/
StringList::StringList(const StringList& original)
{
    // create new list
    head = NULL;
    // point pointer to head of original list
    ListNode *nodePtr = original.head;
    // copy contents of original into new list
    while (nodePtr != NULL)
    {
        add(nodePtr->value);
        nodePtr = nodePtr->next;
    }
}

/******************************************************************************
 *                                 DESTRUCTOR
 * DESCRIPTION: Deletes any memory that was dynamically allocated by the
 * StringList object
 ******************************************************************************/
StringList::~StringList()
{
    ListNode *nodePtr = head;
    ListNode *garbage;
    while (nodePtr != NULL)
    {
        // put current node into garbage
        garbage = nodePtr;
        // point to next node BEFORE destroying current or you will lose link
        nodePtr = nodePtr->next;
        // empty garbage
        delete garbage;
    }
    std::cout << "Destruction has been visited on your list!" << std::endl;
}

/******************************************************************************
 *                                    add()
 * DESCRIPTION: Adds a new node containing the value of the parameter to the end
 * of the list.
 ******************************************************************************/
void StringList::add(std::string val)
{
    if (head == NULL)   // list is empty
        head = new ListNode(val);
    else
    {
        // list is not empty. traverse to end of list
        ListNode *nodePtr = head;
        while (nodePtr->next != NULL)
        {
            nodePtr = nodePtr->next;
        }
        // nodePtr now points to last node
        // add a new node containing val to the end of the list
        nodePtr->next = new ListNode(val);
    }
}

/******************************************************************************
 *                                 postitionOf()
 * DESCRIPTION: Returns the (zero-based) position in the list for the first
 * occurrence of the parameter in the list, or -1 if that value is not in the
 * list.
 ******************************************************************************/
int StringList::positionOf(std::string val)
{
    int pos = -1;       // -1 while not found
    int index = 0;      // node counter
    ListNode *nodePtr = head;

    //while node->value != val && node != NULL
    while ((pos == -1) && (nodePtr != NULL))
    {
        if (nodePtr->value == val)
            pos = index;
        index++;
        nodePtr = nodePtr->next;
    }

    // test print
    if (pos == -1)
        std::cout << val << " was not found." << std::endl;
    else
        std::cout << val << " is at index " << pos << std::endl;

    return pos;
}

/******************************************************************************
 *                                 setNodeVal()
 * DESCRIPTION: The first parameter represents a (zero-based) position in the
 * list.  The setNodeVal() function sets the value of the node at that position
 * to the value of the string parameter.  If the position parameter is >= the
 * number of nodes in the list, the operation cannot be carried out and
 * setNodeVal() should return false, otherwise it should be successful and
 * return true.
 ******************************************************************************/
bool StringList::setNodeVal(int pos, std::string val)
{
    // flag
    bool isValidNode = false;
    int index = 0;      // node counter
    // set iterator to head of list
    ListNode *nodePtr = head;

    // move to the node at pos
    while (index != pos && nodePtr != NULL)
    {
        index++;
        nodePtr = nodePtr->next;
    }

    if (nodePtr != NULL)    // pos is valid node
    {
        // set nodeAtPos->value to val and return true
        nodePtr->value = val;
        isValidNode = true;
    }

    return isValidNode;
}

/******************************************************************************
 *                                 getAsVector()
 * DESCRIPTION: Returns a vector with the same size, values and order as the
 * StringList.
 ******************************************************************************/
std::vector<std::string> StringList::getAsVector()
{
    // create a new vector
    std::vector<std::string> stringVec;
    // set iterator to head of list
    ListNode *nodePtr = head;

    // insert values from StringList into vector in order
    while (nodePtr != NULL)
    {
        stringVec.push_back(nodePtr->value);
        nodePtr = nodePtr->next;
    }

    return stringVec;
}

///******************************************************************************
// * displayList - FOR TESTING ONLY
// ******************************************************************************/
//void StringList::displayList() const
//{
//    ListNode *nodePtr = head;
//    while (nodePtr)
//    {
//        std::cout << nodePtr->value << std::endl;
//        nodePtr = nodePtr->next;
//    }
//}

