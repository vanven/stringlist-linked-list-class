/******************************************************************************
 * AUTHOR: Maria Vanessa Vengco <vengcom@oregonstate.edu>
 * DATE: November 22, 2015
 * FILENAME: StringList.hpp
 * DESCRIPTION: Project 9.a - The interface/declaration file for the StringList
 * Class. It defines a simple linked list class, using an appropriate definition 
 * of ListNode, to create and manipulate a list of strings. 
 ******************************************************************************/

#ifndef STRINGLIST_HPP
#define STRINGLIST_HPP

#include <string>
#include <vector>

class StringList
{
    private:
        struct ListNode
        {
            std::string value;
            ListNode *next;
            // ListNode constructor
            ListNode(std::string val, ListNode *nextNode = NULL)
            {
                value = val;
                next = nextNode;
            }
        };
        ListNode *head;         // List head pointer

    public:
        // a default constructor with member initialization list
        StringList();   // : head(NULL) {}
        // a copy constructor
        StringList(const StringList&);  // deep copy??
        // a destructor
        ~StringList();
        // other member functions
        void add (std::string);
        int positionOf (std::string);
        bool setNodeVal(int, std::string);
        std::vector<std::string> getAsVector();

        //void displayList() const;       // for testing only
};

#endif
